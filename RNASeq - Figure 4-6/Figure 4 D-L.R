library(ggplot2)
library(edgeR)
library(ape)
library(gridExtra)
library(sva)
library(ComplexHeatmap)
library(circlize)
library(ggpubr)
library(biomaRt)

##################################################################


## Annotating gene ids with BiomaRt

#ensembl=useMart("ensembl")
#ensembl = useDataset("hsapiens_gene_ensembl",mart=ensembl)
#attributes = listAttributes(ensembl)
#ensids=rownames(raw_cnts)
#ensembl_data.df = getBM(attributes=c('external_gene_name','ensembl_gene_id','gene_biotype','entrezgene_id','description'), 
#                 filters = 'ensembl_gene_id', 
#                 values = ensids, 
#                 mart = ensembl)
#ensembl_data.df = ensembl_data.df[!duplicated(ensembl_data.df$ensembl_gene_id),]
#rownames(ensembl_data.df) = ensembl_data.df$ensembl_gene_id
#head(ensembl_data.df)
#ensembl_data.df = ensembl_data.df[!duplicated(ensembl_data.df$ensembl_gene_id),]
#rownames(ensembl_data.df) = ensembl_data.df$ensembl_gene_id
#saveRDS(ensembl_data.df,"ensembl_data.df.RDS")



ensembl_data.df = readRDS("ensembl_data.df.RDS")

##################################################################


##################################################################


timepoint_cols = c("Healthy_Control" =  "#597dba","12_W.P.I." = "#616161", "16_W.P.I." = "#ab2824",  "24_W.P.I." = "#ffb400")



######## Read in files ######## 
raw_cnts = read.delim("featureCounts_merged.txt",sep="\t",row.names = 1,check.names=FALSE)
meta_data = read.csv("meta_data.csv",stringsAsFactors=FALSE)
rownames(meta_data) = meta_data$SampleID


table(rownames(meta_data) %in% colnames(raw_cnts))
meta_data = meta_data[rownames(meta_data) %in% colnames(raw_cnts),]
meta_data = meta_data[meta_data$Timepoint %in% c("Healthy_Control","12_W.P.I.","16_W.P.I.","24_W.P.I."),]
meta_data$Timepoint = factor(meta_data$Timepoint,levels = c("Healthy_Control","12_W.P.I.","16_W.P.I.","24_W.P.I."))

raw_cnts = raw_cnts[,rownames(meta_data)]


##################################################################


######## Impact of con vs No con ######## 
dim(raw_cnts)
dim(meta_data)


y = DGEList(counts=raw_cnts)
keep <- rowSums(cpm(y)>3) > 15
table(keep)
y <- y[keep, , keep.lib.sizes=FALSE]
tokeep = rownames(y)
y = calcNormFactors(y,method = "TMM") 
design = model.matrix(~ Sex  + Run + Timepoint ,meta_data)
y = estimateDisp(y, design, robust=TRUE)
fit <- glmQLFit(y, design)
colnames(design)
lcpm <- cpm(y, log=TRUE,normalized.lib.sizes = TRUE)


#Impact of Group 
qlf <- glmQLFTest(fit,coef = "Timepoint12_W.P.I.")
qlf = topTags(qlf,n=10000)
results_12_wpi = qlf$table
results_12_wpi = results_12_wpi[order(results_12_wpi$FDR,decreasing = FALSE),]
results_12_wpi = data.frame(results_12_wpi,ensembl_data.df[rownames(results_12_wpi),])
sig_12_wpi_fdr = results_12_wpi[(results_12_wpi$FDR < 0.05 ),]
sig_12_wpi_fc = sig_12_wpi_fdr[(abs(sig_12_wpi_fdr$logFC) > log2(1.25)),]
dim(sig_12_wpi_fdr)
dim(sig_12_wpi_fc)

#Impact of Group 
qlf <- glmQLFTest(fit,coef = "Timepoint16_W.P.I.")
qlf = topTags(qlf,n=10000)
results_16_wpi = qlf$table
results_16_wpi = results_16_wpi[order(results_16_wpi$FDR,decreasing = FALSE),]
results_16_wpi = data.frame(results_16_wpi,ensembl_data.df[rownames(results_16_wpi),])
sig_16_wpi_fdr = results_16_wpi[(results_16_wpi$FDR < 0.05 ),]
sig_16_wpi_fc = sig_16_wpi_fdr[(abs(sig_16_wpi_fdr$logFC) > log2(1.25)),]

dim(sig_16_wpi_fdr)
dim(sig_16_wpi_fc)



#Impact of Group 
qlf <- glmQLFTest(fit,coef = "Timepoint24_W.P.I.")
qlf = topTags(qlf,n=10000)
results_24_wpi = qlf$table
results_24_wpi = results_24_wpi[order(results_24_wpi$FDR,decreasing = FALSE),]
results_24_wpi = data.frame(results_24_wpi,ensembl_data.df[rownames(results_24_wpi),])
sig_24_wpi_fdr = results_24_wpi[(results_24_wpi$FDR < 0.05 ),]
sig_24_wpi_fc = sig_24_wpi_fdr[(abs(sig_24_wpi_fdr$logFC) > log2(1.25)),]
dim(sig_24_wpi_fdr)
dim(sig_24_wpi_fc)

#### Barplot in Figure 4D ####

barplot_data.df = data.frame(Timepoint = c("12_W.P.I.","16_W.P.I.","24_W.P.I."),
                             Num_DE_genes = c(nrow(sig_12_wpi_fc),nrow(sig_16_wpi_fc),nrow(sig_24_wpi_fc)))

barplot_data.df$Timepoint = factor(barplot_data.df$Timepoint,levels=(c("12_W.P.I.","16_W.P.I.","24_W.P.I.")))
barplot_data.df2 = barplot_data.df
barplot_data.df2$Timepoint = factor(barplot_data.df2$Timepoint,levels=(c("12_W.P.I.","16_W.P.I.","24_W.P.I.")))

 
ggplot(barplot_data.df,aes(y=Timepoint,x=Num_DE_genes,fill=Timepoint))+geom_col(width=0.5,colour="black")+scale_fill_manual(values=timepoint_cols)+guides(fill=FALSE)+theme_classic()+
    theme(axis.title = element_blank(),axis.text.x = element_text(size=rel(1.5),face="bold",angle=45,hjust=1,vjust=1))+ggtitle("Number of DE genes FDR < 0.5 and fold change")+coord_flip()







#### Heatplot in Figure 4E ####

fdr_genes = c(
  rownames(sig_12_wpi_fdr[]),
  rownames(sig_16_wpi_fdr[]),
  rownames(sig_24_wpi_fdr[])
)



fdr_genes = c(
  rownames(sig_12_wpi_fdr[]),
  rownames(sig_16_wpi_fdr[]),
  rownames(sig_24_wpi_fdr[])
)



xx = table(fdr_genes)
fdr_genes = names(xx[xx>1])
fdr_genes = unique(fdr_genes)
fdr_genes = fdr_genes[fdr_genes %in% rownames(lcpm)]


fc_genes = c(
  rownames(sig_12_wpi_fc[abs(sig_12_wpi_fc$logFC) > 0.32,]),
  rownames(sig_16_wpi_fc[abs(sig_16_wpi_fc$logFC) > 0.32,]),
  rownames(sig_24_wpi_fc[abs(sig_24_wpi_fc$logFC) > 0.32,])
)
fc_genes = unique(fc_genes)
fc_genes = fc_genes[fc_genes %in% fdr_genes]


highly_variable_lcpm <- lcpm[fc_genes,]
zzz <- t(scale(t(highly_variable_lcpm))) #Scale to Z scores by Row. 
zzz <- pmin(pmax(zzz, -3), 3)


Age_col_fun = colorRamp2(c(20,40,60,80), c("white","darkseagreen2","forestgreen","gray14"))

meta_data$Weeks_Post_Infection = meta_data$WPI
group_colours = c("Healthy" =  "#597dba",  "Mild/Moderate" = "#9463bd",  "Severe/Critical" = "#fb8713",
                  "Healthy_Control" =  "#597dba","12_W.P.I." = "#616161", "16_W.P.I." = "#ab2824",  "24_W.P.I." = "#ffb400")


ha = HeatmapAnnotation(df = meta_data[,c("Severity","Timepoint","Age","Sex")],
                       col = list(Severity = group_colours,
                                  Timepoint = group_colours,
                                  Sex = c("Male" = "goldenrod","Female" = "dodgerblue"),
                                  Age = Age_col_fun))

Heatmap(zzz,show_row_names = FALSE,show_column_names = FALSE,
        top_annotation = ha,col=c("blue3","skyblue3","skyblue1","white","sienna1","sienna2","red3"),
        column_title="DE gene heatplot",heatmap_legend_param = list(title = "Z-score (Log CPM)"))




##### Over representation analysis in Figure 4F-G #####

w12_up = rownames(sig_12_wpi_fc[sig_12_wpi_fc$logFC > 0,])
w12_down = rownames(sig_12_wpi_fc[sig_12_wpi_fc$logFC < 0,])

w16_up = rownames(sig_16_wpi_fc[sig_16_wpi_fc$logFC > 0,])
w16_down = rownames(sig_16_wpi_fc[sig_16_wpi_fc$logFC < 0,])

w24_up = rownames(sig_24_wpi_fc[sig_24_wpi_fc$logFC > 0,])
w24_down = rownames(sig_24_wpi_fc[sig_24_wpi_fc$logFC < 0,])



source("Over_representation_analysis_function_human_Jan_2021.R")
w12_up_path_res = over_represent(gene_list_to_test = w12_up,vector_all_detected_genes = rownames(y),filename = "Pathway w12_up")
w16_up_path_res = over_represent(gene_list_to_test = w16_up,vector_all_detected_genes = rownames(y),filename = "Pathway w16_up")
w24_up_path_res = over_represent(gene_list_to_test = w24_up,vector_all_detected_genes = rownames(y),filename = "Pathway w24_up")

w12_down_path_res = over_represent(gene_list_to_test = w12_down,vector_all_detected_genes = rownames(y),filename = "Pathway w12_down")
w16_down_path_res = over_represent(gene_list_to_test = w16_down,vector_all_detected_genes = rownames(y),filename = "Pathway w16_down")
w24_down_path_res = over_represent(gene_list_to_test = w24_down,vector_all_detected_genes = rownames(y),filename = "Pathway w16_down")


#### Boxplots in Figure H-L ####
adj_lcpm = readRDS("SVASeq_counts.RDS")

circ.df = data.frame(meta_data,t(adj_lcpm[,rownames(meta_data)]))
circ.df$Group4 = paste(circ.df$Timepoint,circ.df$Severity)
circ.df$Group4[circ.df$Group4=="Healthy_Control Healthy"] = "HC"


circ.df$Group4 = factor(circ.df$Group4,levels=c("HC",
                                                "12_W.P.I. Mild/Moderate","12_W.P.I. Severe/Critical",
                                                "16_W.P.I. Mild/Moderate","16_W.P.I. Severe/Critical",
                                                "24_W.P.I. Mild/Moderate","24_W.P.I. Severe/Critical"
                                                
))
soi2 = c("ENSG00000143546","ENSG00000189403","ENSG00000163512","ENSG00000145649","ENSG00000251562")
ensembl_data.df[soi2,]

myc = list(c("HC","12_W.P.I. Mild/Moderate"),c("HC","16_W.P.I. Mild/Moderate"),c("HC","24_W.P.I. Mild/Moderate"))

grid.arrange(  
  ggplot(circ.df,aes_string(x="Group4",y=soi2[1],colour="Severity"))+stat_boxplot(width=0.25,geom = 'errorbar',lwd=0.1)+
    geom_boxplot(fatten=5,width=0.5,lwd=0.1,outlier.shape = NA)+
    geom_jitter(width=0.1,size=1,aes(shape=Severity))+ylab("combat adjusted log2 CPM")+scale_shape_manual(values=c(19,17,6))+
    scale_colour_manual(values=group_colours)+ggtitle(ensembl_data.df[soi2[1],"external_gene_name"])+
    theme_classic()+guides(fill=FALSE,colour=FALSE,shape=FALSE)+stat_compare_means(comparisons=myc)+
    theme(axis.title.y=element_blank(),axis.text.y=element_text(colour="black",size=rel(0.5)),
          axis.title.x = element_blank(),axis.text.x=element_text(face="bold",colour="black",size=rel(0.5))),
  
  ggplot(circ.df,aes_string(x="Group4",y=soi2[2],colour="Severity"))+stat_boxplot(width=0.25,geom = 'errorbar',lwd=0.1)+
    geom_boxplot(fatten=5,width=0.5,lwd=0.1,outlier.shape = NA)+
    geom_jitter(width=0.1,size=1,aes(shape=Severity))+ylab("combat adjusted log2 CPM")+scale_shape_manual(values=c(19,17,6))+
    scale_colour_manual(values=group_colours)+ggtitle(ensembl_data.df[soi2[2],"external_gene_name"])+
    theme_classic()+guides(fill=FALSE,colour=FALSE,shape=FALSE)+stat_compare_means(comparisons=myc)+
    theme(axis.title.y=element_blank(),axis.text.y=element_text(colour="black",size=rel(0.5)),
          axis.title.x = element_blank(),axis.text.x=element_text(face="bold",colour="black",size=rel(0.5))),
  
  ggplot(circ.df,aes_string(x="Group4",y=soi2[3],colour="Severity"))+stat_boxplot(width=0.25,geom = 'errorbar',lwd=0.1)+
    geom_boxplot(fatten=5,width=0.5,lwd=0.1,outlier.shape = NA)+
    geom_jitter(width=0.1,size=1,aes(shape=Severity))+ylab("combat adjusted log2 CPM")+scale_shape_manual(values=c(19,17,6))+
    scale_colour_manual(values=group_colours)+ggtitle(ensembl_data.df[soi2[3],"external_gene_name"])+
    theme_classic()+guides(fill=FALSE,colour=FALSE,shape=FALSE)+stat_compare_means(comparisons=myc)+
    theme(axis.title.y=element_blank(),axis.text.y=element_text(colour="black",size=rel(0.5)),
          axis.title.x = element_blank(),axis.text.x=element_text(face="bold",colour="black",size=rel(0.5))),
  
  ggplot(circ.df,aes_string(x="Group4",y=soi2[4],colour="Severity"))+stat_boxplot(width=0.25,geom = 'errorbar',lwd=0.1)+
    geom_boxplot(fatten=5,width=0.5,lwd=0.1,outlier.shape = NA)+
    geom_jitter(width=0.1,size=1,aes(shape=Severity))+ylab("combat adjusted log2 CPM")+scale_shape_manual(values=c(19,17,6))+
    scale_colour_manual(values=group_colours)+ggtitle(ensembl_data.df[soi2[4],"external_gene_name"])+
    theme_classic()+guides(fill=FALSE,colour=FALSE,shape=FALSE)+stat_compare_means(comparisons=myc)+
    theme(axis.title.y=element_blank(),axis.text.y=element_text(colour="black",size=rel(0.5)),
          axis.title.x = element_blank(),axis.text.x=element_text(face="bold",colour="black",size=rel(0.5))),
  
  
  ggplot(circ.df,aes_string(x="Group4",y=soi2[5],colour="Severity"))+stat_boxplot(width=0.25,geom = 'errorbar',lwd=0.1)+
    geom_boxplot(fatten=5,width=0.5,lwd=0.1,outlier.shape = NA)+
    geom_jitter(width=0.1,size=1,aes(shape=Severity))+ylab("combat adjusted log2 CPM")+scale_shape_manual(values=c(19,17,6))+
    scale_colour_manual(values=group_colours)+ggtitle(ensembl_data.df[soi2[5],"external_gene_name"])+
    theme_classic()+guides(fill=FALSE,colour=FALSE,shape=FALSE)+stat_compare_means(comparisons=myc)+
    theme(axis.title.y=element_blank(),axis.text.y=element_text(colour="black",size=rel(0.5)),
          axis.title.x = element_blank(),axis.text.x=element_text(face="bold",colour="black",size=rel(0.5))),
  
  nrow=1,ncol=5)
