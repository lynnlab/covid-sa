# README #

This is a repository for the analysis of the RNA-Seq, serology and flow cytometry data from the Ryan et al. "Long-term perturbation of the peripheral immune system months after SARS-CoV-2 infection." manuscript.